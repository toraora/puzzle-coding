v = '''I	A	U	E	E	E
A	E	U	O	A	E	O	O
E	I	O	A	E	O	I	U
E	U	I	O	A	U	O	E
O	A	E	U	I
E	A	U
I	A	E	A	A	E	E	O
A	O	E	E	E	O	E	I
U	A	E
O	O	E	E	O	O	I	E	O
E	A	A	U	O	E	E
I	I	U	U
E	E	I	A	I	I	I'''.lower()

c = '''L	R	S	C	L	K	D
R	R	M	N	S
P	D	G	T	H
L	S	W	V	R
C	T	N	T	S	N	T	B
T	R	D	D	C	N	T	J	B	S
N	R	S	T	G
T	H	D	N	F
S	N	T	S	L	S	W	M	N	R
P	T	C	G
R	G	N	M	N	T
B	N	D	R	G	S	R	R	S
G	N	H	L	S	T'''.lower()

vowels = 'aeiou'
cons = 'bcdfghjklmnpqrstvwxyz'

v = [''.join([ch for ch in ss.split('\t')]) for ss in v.split('\n')]
c = [''.join([ch for ch in ss.split('\t')]) for ss in c.split('\n')]

n = 13

w = input()

for i in range(n - len(w) + 1):
    ok = True
    ok_rev = True
    for j in range(len(w)):
        ch = w[j]
        s = v
        if ch not in vowels:
            s = c
        ok = ok and ch in s[i+j]
        ok_rev = ok_rev and ch in s[n-1-j-i]
    if ok:
        print('for: ', i)
    if ok_rev:
        print('rev: ', i)
