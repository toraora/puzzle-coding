from collections import defaultdict

class Loot:
    def __init__(self, items, level):
        self.items = items
        self.level = level

    def __str__(self):
        return str(self.items) + "(level %d)" % self.level

    def __repr__(self):
        return str(self.items) + "(level %d)" % self.level

tot_count = defaultdict(int)
tot_count_per_animal = defaultdict(lambda: defaultdict(int))
seen = set()
to_print = []

level_dict = defaultdict(set)

def parse(line):
    line = line.strip()
    level = int(line.split()[-1][:-1])
    line = line.split('(')[0]
    items = line.split()
    if items[-1] != 'wolf':
        pass
        #return None
    #    return None
    # Include "of the"
    assert len(items) == 7
    # Now remove
    tot_count[level] += 1
    tot_count_per_animal[''][level] += 1

    if line not in seen and level == 56:
        to_print.append(line)
    seen.add(line)

    items = items[:4] + items[6:]

    s = str(items)
    level_dict[s].add(level)

    return Loot(items, level)

def unique(loot_list, index):
    s = defaultdict(int)
    for loot in loot_list:
        s[loot.items[index]] += 1
    return s

if __name__ == '__main__':
    with open('loot') as f:
        lines = f.readlines()
        lines = [line.strip() for line in lines if line.strip()]
        # Ignore header
        lines = lines[1:]
        loot_list = [parse(line) for line in lines if parse(line)]
        sets = [unique(loot_list, i) for i in range(5)]

        """
        for s in sets:
             l = sorted([(n, v) for v,n in s.items()])
             print(len(l))
             print('\n'.join(['{0}\t{1}'.format(n,v) for n,v in l]))
             print('')
        """

        l = sorted([(v, n) for v,n in tot_count.items()])
        print(len(tot_count))
        print('\n'.join(['{0}\t{1}'.format(n,v) for n,v in l]))
        #print('\n'.join(sorted(to_print)))


        for animal, cnts in tot_count_per_animal.items():
            print(animal)
            for i in range(0,6):
                s = ''
                for j in range(1,8):
                    n = 10 * i + j
                    cnt = cnts[n]
                    if cnt < 2000:
                        s += ' '
                    elif cnt < 5500:
                        s += '.'
                    else:
                        s += '#'
                print(s)


        l = sorted([(len(s), s, k) for k,s in level_dict.items()])
        for cnt, ss, key in (l[-100:]):
            print(key, cnt, ss)