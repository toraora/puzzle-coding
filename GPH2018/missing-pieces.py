from collections import defaultdict

"""blue
h1 = ['?6?542']
h2 = ['?--','4--','?31','--5']
h3 = ['?','6','5','?','3','4']
h4 = ['?5','26','?3']
pieces = [
    h1,h2,h3,h4,
    ['2136','??--'],
    ['?2?45','6----'],
    ['6?-','3?1','5--'],
    ['34--','-1?-','--?5'],
    ['?1-','3?6','--4'],
    ['?--','215','--?','--3'],
    ['-4','-1','-?','?2','-6'],
    ['1--','??-','426'],
    ['?--','436','-?2'],
    ['5?61?3'],
    ['--?--','13?24'],
    ['----?','4?165']
]"""

'''green'''
h1 = ['?3251?']
h2 = ['?','?','5','6','4','1']
h3 = ['?634','2--?']
h4 = ['?-','63','?-','1-','4-']
pieces = [
    h1,h2,h3,h4,
    ['1--3','?42?'],
    ['23?','6?1'],
    ['451??','3----'],
    ['5----','?6?23'],
    ['-4--','-?16','?5--'],
    ['?--','256','-1?'],
    ['?4','-2','63','-?'],
    ['4--','?--','1--','?25'],
    ['6?','-1','-4','-?','-3'],
    ['1-','?-','46','?-','5-'],
    ['12','??','-3','-4'],
    ['--35','?1?4']
]

'''red
h1 = ['?6?51','----4']
h2 = ['?46','5--','2--','?--']
h3 = ['?6?135']
h4 = ['?','2','3','?','1','6']
pieces = [
    h1,h2,h3,h4,
    ['4','?','2','?','1','3'],
    ['?1','-?','-6','-3','-2'],
    ['--352','??1--'],
    ['51?2','--?-','--6-'],
    ['?6--','35?1'],
    ['2136','?--?'],
    ['-??-','3546'],
    ['-3','-5','?1','?2'],
    ['36','4?','?5'],
    ['13','?-','4-','?-','6-'],
    ['6?314','?----'],
    ['--5','--4','-?6','2?-']
]'''


def make_tup_from_grid(grid, h):
    rows = [set([i for i in row if i > 0]) for row in grid]
    cols = [set([grid[r][c] for r in range(6) if grid[r][c] > 0]) for c in range(6)]
    return (grid, h, rows, cols)

def make_tup_from_hex(h):
    grid = [[0 for _ in range(6)] for __ in range(6)]
    for rr,r in enumerate(h):
        for cc,c in enumerate(r):
            if c != '-':
                grid[rr][cc] = -1 if c == '?' else int(c)
    return make_tup_from_grid(grid, h)

grids = [
    make_tup_from_hex(h1),
    make_tup_from_hex(h2),
    make_tup_from_hex(h3),
    make_tup_from_hex(h4),
]

def solve_sudoku(grid, used):
    hex_cons = defaultdict(list)
    for k,v in used.items():
        down,across = v
        piece = pieces[k]
        first = None
        for row in piece:
            cur_across = across
            for c in row:
                if c != '-':
                    if first is None:
                        first = (down, cur_across)
                    hex_cons[first].append((down, cur_across))
                    hex_cons[(down,cur_across)] = hex_cons[first]
                cur_across += 1
            down += 1

    grid = [[0 if i <= 0 else i for i in row] for row in grid]
    candidates = [[set(range(1,7)) for _ in range(6)] for __ in range(6)]
    left = 36
    for r in range(6):
        for c in range(6):
            cur = grid[r][c]
            if cur == 0:
                continue
            left -= 1
            candidates[r][c] = set()
            for k in range(6):
                candidates[r][k].discard(cur)
                candidates[k][c].discard(cur)
                if (r,c) in hex_cons:
                    hex_con = hex_cons[(r,c)][k]
                    try:
                        candidates[hex_con[0]][hex_con[1]].discard(cur)
                    except:
                        print(hex_cons)
                        raise

    res = re_s(grid, candidates, hex_cons, left)
    if res:
        for rr,r in enumerate(res):
            s = ''
            for cc,c in enumerate(r):
                if (rr,cc) in hex_cons:
                    s += '\t#'
                else:
                    s += '\t.'
            print(s)
    return res

def re_s(grid, candidates, hex_cons, left):
    cont = True
    while cont:
        cont = False
        for r in range(6):
            for c in range(6):
                if len(candidates[r][c]) == 1:
                    cont = True
                    cur = list(candidates[r][c])[0]
                    grid[r][c] = cur
                    left -= 1
                    for k in range(6):
                        candidates[r][k].discard(cur)
                        candidates[k][c].discard(cur)
                        if (r,c) in hex_cons:
                            hex_con = hex_cons[(r,c)][k]
                            candidates[hex_con[0]][hex_con[1]].discard(cur)

    if left == 0:
        return grid

    for r in range(6):
        for c in range(6):
            if len(candidates[r][c]) == 0:
                continue
            temp = candidates[r][c]
            candidates[r][c] = set()
            for cand in list(temp):
                grid[r][c] = cand
                re_add = []
                for k in range(6):
                    if cand in candidates[r][k]:
                        re_add.append((r,k))
                        candidates[r][k].remove(cand)
                    if cand in candidates[k][c]:
                        re_add.append((k,c))
                        candidates[k][c].remove(cand)
                    if (r,c) in hex_cons:
                        hex_con = hex_cons[(r,c)][k]
                        if cand in candidates[hex_con[0]][hex_con[1]]:
                            re_add.append(hex_con)
                            candidates[hex_con[0]][hex_con[1]].remove(cand)

                res = re_s(grid, candidates, hex_cons, left - 1)
                if res:
                    return res

                for rr,cc in re_add:
                    candidates[rr][cc].add(cand)
                grid[r][c] = 0
            candidates[r][c] = temp

def fill_grid(grid, rows, cols, piece, across, down):
    cur_down = down
    for row in piece:
        cur_across = across
        for c in row:
            if c != '-':
                if grid[cur_down][cur_across] is not 0:
                    return None
                elif c is not '?':
                    if int(c) in rows[cur_down] or int(c) in cols[cur_across]:
                        return None
            cur_across += 1
        cur_down += 1

    for row in piece:
        cur_across = across
        for c in row:
            if c != '-':
                fill = -1 if c is '?' else int(c)
                grid[down][cur_across] = fill
                if fill is not -1:
                    rows[down].add(fill)
                    cols[cur_across].add(fill)
            cur_across += 1
        down += 1
    return grid

def unfill_grid(grid, rows, cols, piece, across, down):
    for row in piece:
        cur_across = across
        for c in row:
            if c != '-':
                grid[down][cur_across] = 0
                if c is not '?':
                    rows[down].remove(int(c))
                    cols[cur_across].remove(int(c))
            cur_across += 1
        down += 1

DIRS = [(0,1),(1,0),(-1,0),(0,-1)]
def are_zeros_hexominoes(grid):
    to_visit = []
    visited = set()
    for r, row in enumerate(grid):
        for c, ch in enumerate(row):
            if (r,c) in visited:
                continue
            if ch != 0:
                visited.add((r,c))
            elif ch == 0:
                seen = 0
                to_visit.append((r,c))
                while to_visit:
                    cur = to_visit.pop()
                    if cur in visited:
                        continue
                    seen += 1
                    visited.add(cur)
                    for dr,dc in DIRS:
                        nr = cur[0] + dr
                        nc = cur[1] + dc
                        if nr >= 0 and nr < 6 and nc >= 0 and nc < 6:
                            if grid[nr][nc] == 0 and (nr, nc) not in visited:
                                to_visit.append((nr,nc))
                if seen % 6 is not 0:
                    return False
    return True

def format_grid(grid, delim='\t'):
    if not grid:
        return None
    return '\n'.join([delim.join([str(s) if s >= 0 else '*' for s in r]) for r in grid])

def re(grid, rows, cols, depth, used, grid_idx):
    if depth == 0:
        if grid_idx < len(grids):
            if not grid or (are_zeros_hexominoes(grid) and solve_sudoku(grid, used[grid_idx-1])):
                g,h,r,c = grids[grid_idx]
                re(g,r,c, 3, used, grid_idx + 1)
        else:
            if are_zeros_hexominoes(grid) and solve_sudoku(grid, used[grid_idx-1]):
                print('########')
                i = 0
                for g,h,r,c in grids:
                    res = solve_sudoku(g,used[i])
                    if res:
                        print (format_grid(res))
                    else:
                        print (format_grid(g))
                    i += 1
                print('########\n')
                exit()
        return

    for i, piece in enumerate(pieces):
        skip = False
        for v in used.values():
            if i in v:
                skip = True
                break
        if skip:
            continue

        height = len(piece)
        width = len(piece[0])

        for across in range(0, 7 - width):
            for down in range(0, 7 - height):
                used[grid_idx-1][i] = (down,across)

                if fill_grid(grid, rows, cols, piece, across, down):
                    re(grid, rows, cols, depth - 1, used, grid_idx)
                    unfill_grid(grid, rows, cols, piece, across, down)

        del used[grid_idx-1][i]

the_used = {
    0:{0:(0,0)},
    1:{1:(0,0)},
    2:{2:(0,0)},
    3:{3:(0,0)}
}

re(None, None, None, 0, the_used, 0)