the_nums = [
    314159265,
    358979323,
    846264338,
    327950288
]

# the_nums = [n // 1000 + 1 for n in the_nums]

ps = {
    0:	9376,
    1:	663,
    2:	896,
    3:	84733,
    4:	3687,
    5:	3483,
    6:	749,
    7:	73836,
    8:	34448,
    9:	6463
}

import bisect

accept_nums = []
for n1 in ps.values():
    for n2 in ps.values():
        for n3 in ps.values():
            for n4 in ps.values():
                accept_nums.append(int(str(n1)))
                accept_nums.append(int(str(n1)+str(n2)))
                accept_nums.append(int(str(n1)+str(n2)+str(n3)))
                accept_nums.append(int(str(n1)+str(n2)+str(n3)+str(n4)))
accept_nums = sorted(accept_nums)
# accept_nums = set(accept_nums)

def steal(nums, i):
    m = nums.index(max(nums))
    cnt = nums[m] // 2
    nums[m] -= cnt
    nums[i] += cnt

def give(nums, i, j):
    nums[j] += nums[i]

ops = {
    #'steal_0': lambda nums: steal(nums, 0),
    #'steal_1': lambda nums: steal(nums, 1),
    #'steal_2': lambda nums: steal(nums, 2),
    #'steal_3': lambda nums: steal(nums, 3),
    #'give_0_1': lambda nums: give(nums, 0, 1),
    #'give_0_2': lambda nums: give(nums, 0, 2),
    #'give_0_3': lambda nums: give(nums, 0, 3),
    #'give_1_2': lambda nums: give(nums, 1, 2),
    #'give_1_3': lambda nums: give(nums, 1, 3),
    #'give_2_3': lambda nums: give(nums, 2, 3),
    'give_1_0': lambda nums: give(nums, 1, 0),
    'give_2_0': lambda nums: give(nums, 2, 0),
    'give_3_0': lambda nums: give(nums, 3, 0),
    #'give_2_1': lambda nums: give(nums, 2, 1),
    #'give_3_1': lambda nums: give(nums, 3, 1),
    #'give_3_2': lambda nums: give(nums, 3, 2)
}

#def check(nums):
#    ok = 0
#    for n in nums:
#        target = bisect.bisect_left(accept_nums, n)
#        if target == len(accept_nums):
#            continue
#        target = accept_nums[target]
#        if target - n < 200000:
#            pass
#            #print(target, n, target - n)
#        if target - n > 3000:
#            continue
#        ok += 1
#    return ok >= 1

"""
to_visit = [([], the_nums)]
seen = set()
while to_visit:
    cur_ops, cur_nums = to_visit.pop()
    if len(cur_ops) not in seen:
        print('len: ', len(cur_ops))
        seen.add(len(cur_ops))
    #print(cur_ops, cur_nums)
    if check(cur_nums):
        print('####################')
        print('cookies: ', cur_nums)
        print('\n'.join(cur_ops))
        print(len(cur_ops))
        for n in cur_nums:
            target = bisect.bisect_left(accept_nums, n)
            target = accept_nums[target]
            print(target, n, target - n)
        print('####################')
    for k,v in ops.items():
        next_nums = list(cur_nums)
        next_ops = list(cur_ops)
        next_ops.append(k)
        v(next_nums)
        to_visit.insert(0, (next_ops, next_nums))
"""

t = [0,1,2,3]
end_target = 3444834448

def to_bin_array(n):
    k = []
    cur = 0
    for c in bin(n)[2:]:
        b = int(c)
        cur = 2 * cur + b
        k.append(cur)
    return k

import itertools

def finish_sol(target, n, p, i, j, k):
    i = the_nums[t[1]] * i
    j = the_nums[t[2]] * j
    k = the_nums[t[3]] * k

    end_arr = to_bin_array(end_target)

    win_i = None
    win_j = None
    win_k = None

    for perm in itertools.permutations(end_arr):
        s = sum(perm)
        cur_i = i + s
        cur_j = j + s
        cur_k = k + s

        target = bisect.bisect_left(accept_nums, cur_i)
        target = accept_nums[target]
        if target - cur_i < 100:
            print('i: ', target, list(perm))

        target = bisect.bisect_left(accept_nums, cur_j)
        target = accept_nums[target]
        if target - cur_j < 100:
            print('j: ', target, list(perm))

        target = bisect.bisect_left(accept_nums, cur_k)
        target = accept_nums[target]
        if target - cur_k < 100:
            print('k: ', target, list(perm))

    print(end_arr, win_i, win_j, win_k)

#nums = [915716571, 28616142, 894254, 55890][::-1]
nums = [1831433143, 28616142, 894254, 55890][::-1]
def get_close(x):
    for p in range(0, 4):
        start = int(2**p) * x
        for i in range(0, 50):
            i_fact = i * nums[0]
            for j in range(0, 50):
                j_fact = j * nums[1]
                for k in range(0, 50):
                    k_fact = k * nums[2]
                    for l in range(1, 50):
                        n = start + i_fact + j_fact + k_fact + l * nums[3]
                        target = bisect.bisect_left(accept_nums, n)
                        target = accept_nums[target]
                        if target - n < 200:
                            print(target, n, p, i, j, k, l)
                            print(nums[0], i, start + i * nums[0])
                            print(nums[1], j, start + j * nums[1] + i * nums[0])
                            print(nums[2], k, start + k * nums[2] + j * nums[1] + i * nums[0])
                            print(nums[3], l, start + l * nums[3] + k * nums[2] + j * nums[1] + i * nums[0])


'22974676672 13540229408 20988818432'
#print(get_close(22974676672))
#print(get_close(13540229408))
print(get_close(20988818432))

"""
for p in range(3, 4):
    start = int(2**p) * the_nums[t[0]]
    for i in range(1, 100):
        i_fact = i * the_nums[t[1]]
        for j in range(1, 100):
            j_fact = j * the_nums[t[2]]
            for k in range(1, 100):
                n = start + i_fact + j_fact + k * the_nums[t[3]]
                target = bisect.bisect_left(accept_nums, n)
                target = accept_nums[target]
                if target - n < 100:
                    print(target, n, p, i, j, k)
                    print(bin(i), bin(j), bin(k))
                    print(64 * the_nums[t[1]], 16 * the_nums[t[2]], 64 * the_nums[t[3]])
"""


'''
0
9376 (ZERO)


(1) + (4) = 642109553
'''