s = '''step-now-any-onto
no-unless-like
permanents-unless-it’s-own-power-cards-white
greater-by-untap
(5)

basic-now-would
other-itself-white-dark-once-remove
converted-magic-artifacts-enemies-shuffle
player’s-attack-other-other-up-do-gains
discard
until-basic-pay-loses
+1/+1-light-reveal-magic-even
(6)

converted-by-magic-itself-storm-magic
beast-combat-magic-controls-converted-magic-master-hear-top-controller
able-enters-dark-his-it’s-like
up-top
green-magic-any-onto-step-day
white-cards-combat-into
his-up-activated
(5)

enters-home-up-activated-block-she-+1/+1
own-home-blocks-onto
itself-storm-top-his-gets-white-activated-combat-total
remove-day-his-other-into
(4)

any-converted-shuffle-hear-magic
she-allies-protection-day
planeswalker-reveal-she-white-any-power
controls-lands-other-day-enters
green-block-activated-own-she
even-storm-onto
converted-choice-other-shuffle-total-top-hear-magic
(4)

reveals-vigilance-home-magic-upkeep-vigilance-activated-block-any-shuffle-hear-3
sorcery-green-regenerated-able-magic-protection-discards-light-attack-pay-it’s
block-permanent
into-white-activated-onto
(5)

able-onto-step-storm-it’s-would-enemies-discards-magic-remove
black-white-permanent-total-magic-attack-step
any-untap-+1/+1-us-hear-magic-white
until-storm-onto-total-magic-able-no
home-magic-pay-green-choice-top-white
no-magic-remove-vigilance-us-allies-permanents
home-magic-master-cards-his-once'''


words = []
tokens = dict()
ls = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
idx = 0
for l in s.split('\n'):
    if '-' not in l:
        continue

    tok = l.split('-')
    w = ''
    for t in tok:
        if t not in tokens:
            print('new token: ', t)
            tokens[t] = ls[idx]
            idx += 1
        w += tokens[t]

    words.append(w)

print(w)