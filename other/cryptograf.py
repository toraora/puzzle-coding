a = '''卅十一工幺
幺囗乙
七刄卅匕卅
七刄廿
卅乙囗卅冂一廿乙囗
刄卅勺勺匚
厂卅幺囗十
卅乙一乙
厂厂卅幺廿
廿又卅工
卅卄一乙大
廿囗囗幺

十刄又
廿卅刄廿乙匚工
七匚丗
乙一囗冂
卅冈廿卅
七仉又十乙又卄
乙囗囗乙
刄卅廿刄囗
㐅一乙卅囗工勺
厂厂一勺
勺囗十仉
廿刄又幺囗十'''

# b = ''''卅', '十', '一', '工', '幺', '囗', '乙', '七', '刄', '匕', '廿', '冂', '勺', '匚', '厂', '又', '卄', '大', '丗', '冈', '仉', '㐅''''
lens = [4,2,1,3,3,3,1,2,3,2,3,2,3,2,2,2,3,3,4,4,4,2]

letters = 'abcdefghijkmlnopqrstuvwxyz'
idx = 0
letter_dict = dict()

for line in a.split('\n'):
    if not line:
        print('')
        continue
    s = ''
    l = 0
    for c in line:
        if c not in letter_dict:
            letter_dict[c] = (letters[idx], lens[idx])
            idx += 1
        s = letter_dict[c][0] + s
        l += letter_dict[c][1]
    print(line, l)
print(idx)
