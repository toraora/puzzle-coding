grids = [
#    ['antelope','monkey1','sloth','turtle','lion','dog'],
    ['monkey2','tiger','dog','zazu','leopard','croc'],
    ['bird1','cheetah','leopard','rhino','ant','turtle'],
    ['tiger','sloth','mouse','grasshopper','anteater','cheetah'],
    ['bird2','mouse','zazu','ant','bird3','lion'],
    ['bison','tiger','spider','bird2','pumbaa','turtle'],
    ['bird4','croc','hyena','ant','sloth','spider'],
    ['butterfly','bird1','bird2','monkey2','lion_mane','sloth'],
#    ['bird3','antelope','bird1','emu','tiger','hyena'],
#    ['giraffe','bird1','grasshopper','lion','croc','bison'],
    ['grasshopper','hyena','butterfly','zazu','beetle','turtle'],
    ['bird1','mouse','spider','dog','beetle','snake'],
    ['spider','leopard','lion','butterfly','antetaer','emu'],
    ['monkey2','beetle','antelope','bison','ant','anteater'],
    ['bird3','butterfly','bird5','dog','cheetah','bison'],
    ['leopard','grasshopper','snake','bird2','bird5'],
    ['monkey1','hyena','bison','lion_mane','leopard','mouse'],
#    ['monkey1','giraffe','butterfly','snake','ant','tiger'],
    ['zazu','bison','rhino','emu','sloth','snake'],
    ['cheetah','monkey1','bird2','emu','beetle','croc'],
    ['bird5','beetle','rhino','tiger','lion','lion_mane'],
    ['rhino','butterfly','antelope','pumbaa','mouse','croc']
]

grids = [set(g) for g in grids]

def all_pairs(lst):
    if len(lst) < 2:
        yield lst
        return
    a = lst[0]
    for i in range(1,len(lst)):
        pair = (a,lst[i])
        for rest in all_pairs(lst[1:i]+lst[i+1:]):
            yield [pair] + rest

ignore = set([10, 18, 1, 9])
ignored_animals = set(['pumbaa','monkey2','anteater'])

from collections import defaultdict

for pairing in all_pairs(list(range(18))):
    valid = True
    for pair in pairing:
        l1 = grids[pair[0]]
        l2 = grids[pair[1]]
        if not len(l1 & l2):
            valid = False
            break
    if not valid:
        continue
    print(pairing)

def agg(grids, ignore):
    aggs = defaultdict(list)
    for i,animal_list in enumerate(grids):
        if i in ignore:
            continue
        for animal in animal_list:
            if animal in ignored_animals:
                continue
            aggs[animal].append(i)
    return aggs

def get_and_remove(grids, ignore):
    print('')
    my_grids = grids
    aggs = agg(my_grids, ignore)
    for k,v in aggs.items():
        if len(v) == 2:
            print(k, v)
            ignore.add(v[0])
            ignore.add(v[1])
    return grids, ignore

def get_grids_without(animals):
    for i,animal_list in enumerate(grids):
        if i in ignore:
            continue
        for animal in animal_list:
            if animal in animals:
                pass



