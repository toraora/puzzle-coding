ss = ['''lehn
diom
a-ps
turx
''',
'''iaeh
lmdn
px-r
otus
''',
'''adeh
ilmn
txop
u-sr
''',
'''oidh
taem
ulrn
pxs-
''',
'''almd
-ipn
orxe
tush
''']

ss = [s.split('\n') for s in ss]

moves = [
    ['dll', 'drrr', 'ul', 'ddruu', 'll', 'uul'],
    ['ul', 'ddl', 'uu', 'dddll', 'rrrd', 'uuur'],
    ['ldd', 'luuu', 'ru', 'lld', 'rru', 'rrdll'],
    ['dlll', 'urrd', 'u', 'urrr', 'll', 'lld'],
    ['rddd', 'lluuu', 'rr', 'rrdd', 'llluu', 'llluu']
]

DIRS = {
    'd': (-1,0),
    'u': (1,0),
    'r': (0,-1),
    'l': (0,1)
}

import itertools

def print_grid(g):
    for a in g:
        print('.'.join(a))

for s in ss:
    for ms in moves:
        for p in itertools.permutations(ms):
            grid = [[c for c in st] for st in s]
            hyph_idx = ''.join([''.join([a for a in aa]) for aa in grid]).index('-')
            cur = (hyph_idx // 4, hyph_idx % 4)
            ext = ''
            ok = True
            for move in p:
                first = True
                for d in move:
                    di = DIRS[d]
                    nex = (cur[0] + di[0], cur[1] + di[1])
                    if nex[0] < 0 or nex[0] > 3 or nex[1] < 0 or nex[1] > 3:
                        ok = False
                        break
                    if first:
                        ext += grid[nex[0]][nex[1]]
                        first = False
                    grid[nex[0]][nex[1]], grid[cur[0]][cur[1]] = grid[cur[0]][cur[1]], grid[nex[0]][nex[1]]
                    cur = nex
                if not ok:
                    break
            if ok:
                if cur == (3,3):
                    if ''.join([''.join([a for a in aa]) for aa in grid]) == 'adehilmnoprstux-':
                        print(ext)
                        break

